﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormCollections;

namespace FormCollectionsDemonstration
{
    public partial class Form1 : Form
    {
        LinkedListForms _formList = new LinkedListForms();
        private string _cache = @"Cache.xml";
        public Form1()
        {
            InitializeComponent();

            if (File.Exists(_cache))
            { 
                FormXmlCacheManager.Load(_formList);
            
                foreach (var form in _formList)
                {
                    form.Show();
                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            _formList.Clear();
            _formList.AddLast(new Form2());
            _formList.AddLast(new Form3());

            foreach (var form in _formList)
            {
                form.FormClosing += HideIfFormClosing;
                form.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (_formList.Count != 0)
            {
                if ((_formList.First().Visible==false) || (_formList.Last().Visible == false)) return;

                label5.Visible = false;
                FormXmlCacheManager.Save(_formList);
            }
            else
            {
                label5.Visible = true;
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!File.Exists(_cache))
            {
                label4.Visible = true;
                return;
            }
            else
            {
                label4.Visible = false;
            }

            FormXmlCacheManager.Load(_formList);

            foreach (var form in _formList)
            {
                form.FormClosing += HideIfFormClosing;
                form.Visible = true;
            }
        }

        private static void HideIfFormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                ((Form)sender).Hide();
            }
        }
    }
}
