﻿namespace FormCollectionsDemonstration
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(91, 12);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(74, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.Text = "1234";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(171, 28);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(28, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.Text = "567";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(205, 39);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(23, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.Text = "89";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(234, 61);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(23, 20);
            this.textBox6.TabIndex = 5;
            this.textBox6.Text = "0";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(243, 87);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(23, 20);
            this.textBox7.TabIndex = 6;
            this.textBox7.Text = "12";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(243, 113);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(18, 20);
            this.textBox8.TabIndex = 7;
            this.textBox8.Text = "3";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(225, 139);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(26, 20);
            this.textBox9.TabIndex = 8;
            this.textBox9.Text = "45";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(196, 169);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(32, 20);
            this.textBox10.TabIndex = 9;
            this.textBox10.Text = "6";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(163, 185);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(24, 20);
            this.textBox11.TabIndex = 10;
            this.textBox11.Text = "7";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(91, 201);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(56, 20);
            this.textBox12.TabIndex = 11;
            this.textBox12.Text = "89012345";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(43, 185);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(28, 20);
            this.textBox13.TabIndex = 12;
            this.textBox13.Text = "67";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(17, 159);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(26, 20);
            this.textBox14.TabIndex = 13;
            this.textBox14.Text = "8";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(5, 123);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(26, 20);
            this.textBox15.TabIndex = 14;
            this.textBox15.Text = "901";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(6, 87);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(25, 20);
            this.textBox16.TabIndex = 15;
            this.textBox16.Text = "23";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(17, 61);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(14, 20);
            this.textBox17.TabIndex = 16;
            this.textBox17.Text = "4";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(30, 39);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(19, 20);
            this.textBox18.TabIndex = 17;
            this.textBox18.Text = "56";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(55, 28);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(33, 20);
            this.textBox19.TabIndex = 18;
            this.textBox19.Text = "7890";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(55, 75);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(44, 20);
            this.textBox20.TabIndex = 19;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(140, 75);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(47, 20);
            this.textBox21.TabIndex = 20;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(91, 139);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(56, 20);
            this.textBox22.TabIndex = 21;
            // 
            // Form3
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(268, 224);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Name = "Form3";
            this.Text = "Form3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form3_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form3_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
    }
}